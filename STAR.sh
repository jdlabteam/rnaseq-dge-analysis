#!/bin/bash 
#SBATCH --cpus-per-task=12 
#SBATCH --mem=120G 
#SBATCH -J align 
#SBATCH --array=1-12
#SBATCH -o /working directory/logs/star_align.out 
#SBATCH -e /working directory/logs/star_align.err 

module load star 
ID=$( sed -n ${SLURM_ARRAY_TASK_ID}p /working directory/lookup.txt ) 
STAR --genomeDir /STAR genome directory/star-GRCm38_ens97-idx \
     --readFilesIn /working directory/results/post_rrna_filter/${ID}_no_rRNA.fastq.1.gz /working directory/results/post_rrna_filter/${ID}_no_rRNA.fastq.2.gz \
     --runThreadN 12 \
     --readFilesCommand zcat \
     --quantMode TranscriptomeSAM GeneCounts \
     --outSAMstrandField intronMotif \
     --genomeLoad NoSharedMemory \
     --sjdbScore 2 \
     --outSAMunmapped Within \
     --outTmpDir /tmp/STAR-${ID} \
     --outFileNamePrefix /working directory/results/mapped/${ID}_ \
     --outSAMtype BAM SortedByCoordinate 