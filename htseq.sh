#!/bin/bash
#SBATCH --mem=50G
#SBATCH -J htseq_count
#SBATCH -o /working directory/logs/htseq_count.out
#SBATCH -e /working directory/logs/htseq_count.err
#SBATCH --array=1-12

ml htseq

ID=$( sed -n ${SLURM_ARRAY_TASK_ID}p /working directory/lookup.txt )

gff=/STAR genome/genomes/Mus_musculus.GRCm38.97.gtf
alignment_file=/working directory/results/mapped/${ID}_Aligned.sortedByCoord.out.bam
out=/working directory/results/htseq_count/${ID}_htseq.txt
htseq-count -f bam -s no $alignment_file $gff >$out