#!/bin/bash 
#SBATCH --mem=50G 
#SBATCH -J fastqc_after_trim 
#SBATCH --array=1-18
#SBATCH -o /working directory/logs/fastqc_after_trim.out 
#SBATCH -e /working directory/logs/fastqc_after_trim.err 

module load fastqc 
ID=$( sed -n ${SLURM_ARRAY_TASK_ID}p /working directory/lookup.txt ) 
fastqc -o /working directory/results/fastqc_after_trim -f fastq /working directory/results/trimmed/${ID}_trimmed_1P.fastq.gz \
          /working directory/results/trimmed/${ID}_trimmed_2P.fastq.gz 