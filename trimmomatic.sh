#!/bin/bash 
#SBATCH --cpus-per-task=12 
#SBATCH --mem=50G 
#SBATCH -J trim 
#SBATCH --array=1-18
#SBATCH -o /working directory/logs/trim.out 
#SBATCH -e /scratch/jdlab/jiayang/myt1l_E14_rnaseq/logs/trim.err 

module load java 
module load trimmomatic 
ID=$( sed -n ${SLURM_ARRAY_TASK_ID}p /working directory/lookup.txt ) 
java -jar $TRIMMOMATIC_HOME/trimmomatic-0.38.jar PE -phred33 -threads 12 -basein /working directory/raw/${ID}_R1_001.fastq.gz \
     -baseout /scratch/jdlab/jiayang/working directory/trimmed/${ID}_trimmed.fastq.gz \
     ILLUMINACLIP:/adapter directory/TruSeq3-PE.fa:2:30:10 LEADING:20 TRAILING:20 MINLEN:25